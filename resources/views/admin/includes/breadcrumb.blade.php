<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item {{ Request::is('admin') ? 'active' : ''  }}">
        @if(Request::is('admin'))
            Головна
        @else
            <a href="{{ route('admin.dashboard') }}">На головну</a>
        @endif
    </li>
    @yield('breadcrumb-item')
</ol>
